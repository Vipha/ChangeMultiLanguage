package com.example.vipha.vipha;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import utils.AppConstants;


/**
 * @ Created by Huo Chhunleng on 04/Apr/2017.
 */

public class DialogChangeLan extends Dialog implements View.OnClickListener{

    private OnDialogListener listener;
    public static TextView tv_close, tv_confirm, tv_msg1;
    private String selectedLanguage;
    public DialogChangeLan(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_a013_9p);
        tv_close = (TextView) findViewById(R.id.tv_close);
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);
        tv_msg1 = (TextView) findViewById(R.id.tv_msg1);
        tv_close.setOnClickListener(this);
        tv_confirm.setOnClickListener(this);

        super.setCanceledOnTouchOutside(false);
        show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_close :
                dismiss();
                break;

            case R.id.tv_confirm :
                listener.onConfirm();
                dismiss();
                break;
        }
    }

    public interface OnDialogListener{
        void onConfirm();
    }

    public void setOnDialogListener(OnDialogListener listener){
        this.listener = listener;
    }

    public void setButtonText1(String text){
        tv_close.setText(text);
    }

    public void setButtonText2(String text){
        tv_confirm.setText(text);
    }

    public void setMessage(String text){
        tv_msg1.setText(text);
    }

}
