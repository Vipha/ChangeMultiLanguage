package com.example.vipha.vipha;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import java.util.Locale;
import utils.AppConstants;
import utils.LanguageType;
import utils.PrefUtils;

public class IntroSetting extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout btnKorean,btnEnglish,btnChinese,btnJapan;
    private String selectedLanguage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_setting);


        try {
            selectedLanguage= PrefUtils.getLanguage(IntroSetting.this).languageType;
            Log.e("Selected language",selectedLanguage);
        } catch (Exception e) {
            Log.e("eeror",e+"");
            selectedLanguage= AppConstants.ENGLISH;
            e.printStackTrace();
        }
        btnKorean=(LinearLayout)findViewById(R.id.btnKorean);
        btnEnglish=(LinearLayout)findViewById(R.id.btnEnglish);
        btnChinese=(LinearLayout)findViewById(R.id.btnChinese);
        btnJapan=(LinearLayout)findViewById(R.id.btnJapan);

        btnKorean.setOnClickListener(this);
        btnEnglish.setOnClickListener(this);
        btnJapan.setOnClickListener(this);


        btnChinese.setOnClickListener(this);
    }
    public void changeLanguage(){

        LanguageType languageType=new LanguageType();
        if(selectedLanguage.equalsIgnoreCase(AppConstants.ENGLISH)){
            languageType.languageType=AppConstants.ENGLISH;
            PrefUtils.setLanguage(languageType,IntroSetting.this);
            Log.e("Selected language",PrefUtils.getLanguage(IntroSetting.this).languageType);
            Configuration config = new Configuration();
            config.locale = Locale.ENGLISH;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        }else if(selectedLanguage.equalsIgnoreCase(AppConstants.KOREAN)){
            languageType.languageType=AppConstants.KOREAN;
            PrefUtils.setLanguage(languageType,IntroSetting.this);

            Log.e("Selected language",PrefUtils.getLanguage(IntroSetting.this).languageType);
            Configuration config = new Configuration();
            config.locale = Locale.KOREA;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        } else if(selectedLanguage.equalsIgnoreCase(AppConstants.CHINESE)){
            languageType.languageType=AppConstants.CHINESE;
            PrefUtils.setLanguage(languageType,IntroSetting.this);

            Log.e("Selected language",PrefUtils.getLanguage(IntroSetting.this).languageType);
            Configuration config = new Configuration();
            config.locale = Locale.CHINESE;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }
        Intent i=new Intent(IntroSetting.this,TutorialActivity.class);
           startActivity(i);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnKorean:
                selectedLanguage=AppConstants.KOREAN;
                changeLanguage();
                break;
            case R.id.btnEnglish:
                selectedLanguage=AppConstants.ENGLISH;
                changeLanguage();
                break;
            case R.id.btnChinese:
                selectedLanguage=AppConstants.CHINESE;
                changeLanguage();
                break;

        }

    }
}
