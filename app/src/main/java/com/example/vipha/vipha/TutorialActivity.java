package com.example.vipha.vipha;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class TutorialActivity extends AppCompatActivity {

    protected ViewPager viewPager;
    protected MyViewPagerAdapter myViewPagerAdatper;
    private int[] layouts;
    private LinearLayout dotsLayout;
    protected ImageView[] docts;
    protected Button btnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        btnStart = (Button) findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(TutorialActivity.this, Setting.class);
                startActivity(i);
            }
        });

        layouts=new int[]{
                R.layout.intro_1,
                R.layout.intro_2,
                R.layout.intro_3,
                R.layout.intro_4,
                R.layout.intro_5,
                R.layout.intro_6

        };
        Integer[] IconsEng = {
                R.drawable.phone_01,
                R.drawable.phone_02,
                R.drawable.phone_03,
        };
        addButtomDots(0);
        myViewPagerAdatper = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdatper);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addButtomDots(position);
       }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addButtomDots(int position) {
        docts = new ImageView[layouts.length];

        dotsLayout.removeAllViews();
        for(int i = 0; i < docts.length; i++){
            docts[i] = new ImageView(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,0,18,0);
            docts[i].setLayoutParams(layoutParams);
            docts[i].setBackgroundResource(R.drawable.intro_pgn_off_icon);
            dotsLayout.addView(docts[i]);

        }
        if(docts.length > 0){
            docts[position].setBackgroundResource(R.drawable.intro_pgn_on_icon);
        }
    }


    private class MyViewPagerAdapter extends PagerAdapter{
        private LayoutInflater layoutInflater;
        private MyViewPagerAdapter(){


        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
