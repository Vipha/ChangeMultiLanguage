package com.example.vipha.vipha;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import utils.AppConstants;
import utils.LanguageType;
import utils.PrefUtils;

public class Setting2 extends AppCompatActivity implements View.OnClickListener{

    private LinearLayout ll_Korean,ll_Eng,ll_Chi,ll_Japan;
    private String selectedLanguage;
    private DialogChangeLan changelangage;
    TextView selectLan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting2);
        ll_Japan=(LinearLayout)findViewById(R.id.ll_Japan);
        ll_Chi=(LinearLayout)findViewById(R.id.ll_Chinese);
        ll_Eng=(LinearLayout)findViewById(R.id.ll_English);
        ll_Korean=(LinearLayout)findViewById(R.id.ll_Korea);

        selectLan=(TextView) findViewById(R.id.selectlanguage);

        ll_Korean.setOnClickListener(this);
        ll_Eng.setOnClickListener(this);
        ll_Chi.setOnClickListener(this);
        ll_Japan.setOnClickListener(this);

        try {
            selectedLanguage= PrefUtils.getLanguage(Setting2.this).languageType;
            Log.e("Selected language",selectedLanguage);
        } catch (Exception e) {
            Log.e("eeror",e+"");
            selectedLanguage= AppConstants.ENGLISH;
            e.printStackTrace();
        }

    }
    public void changeLanguage(){

        final LanguageType languageType=new LanguageType();
        if(selectedLanguage.equalsIgnoreCase(AppConstants.ENGLISH)){
            languageType.languageType=AppConstants.ENGLISH;
            PrefUtils.setLanguage(languageType,Setting2.this);
            Log.e("Selected language",PrefUtils.getLanguage(Setting2.this).languageType);
            Configuration config = new Configuration();
            config.locale = Locale.ENGLISH;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());

        }else if(selectedLanguage.equalsIgnoreCase(AppConstants.KOREAN)){
            languageType.languageType=AppConstants.KOREAN;
            PrefUtils.setLanguage(languageType,Setting2.this);

            Log.e("Selected language",PrefUtils.getLanguage(Setting2.this).languageType);
            Configuration config = new Configuration();
            config.locale = Locale.KOREA;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        } else if(selectedLanguage.equalsIgnoreCase(AppConstants.CHINESE)){
            languageType.languageType=AppConstants.CHINESE;
            PrefUtils.setLanguage(languageType,Setting2.this);

            Log.e("Selected language",PrefUtils.getLanguage(Setting2.this).languageType);
            Configuration config = new Configuration();
            config.locale = Locale.CHINESE;
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        }
        DialogChangeLan dialogChangeLan=new DialogChangeLan(Setting2.this);
        dialogChangeLan.setOnDialogListener(new DialogChangeLan.OnDialogListener() {
            @Override
            public void onConfirm() {

            }
        });

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_Chinese:
                changeLanguage();
                if(selectedLanguage.equalsIgnoreCase(AppConstants.ENGLISH)){
                    DialogChangeLan.tv_msg1.setText(getResources().getString(R.string.ToChange) + "\t" +getResources().getString(R.string.Chinese));

                }else if(selectedLanguage.equalsIgnoreCase(AppConstants.KOREAN)){
                    DialogChangeLan.tv_msg1.setText(getResources().getString(R.string.ToChange) + "\t" +getResources().getString(R.string.Chinese) + "\t" +getResources().getString(R.string.ToChangeKorea)
                    );

                } else if(selectedLanguage.equalsIgnoreCase(AppConstants.CHINESE)){
                    DialogChangeLan.tv_msg1.setText(getResources().getString(R.string.ToChange) + "\t" +getResources().getString(R.string.Chinese));
                }


              break;
            case R.id.ll_English:
                changeLanguage();
                if(selectedLanguage.equalsIgnoreCase(AppConstants.ENGLISH)){
                    DialogChangeLan.tv_msg1.setText(getResources().getString(R.string.ToChange) + "\t" +getResources().getString(R.string.English));

                }else if(selectedLanguage.equalsIgnoreCase(AppConstants.KOREAN)){
                    DialogChangeLan.tv_msg1.setText(getResources().getString(R.string.ToChange) + "\t" +getResources().getString(R.string.English) + "\t" +getResources().getString(R.string.ToChangeKorea)
                    );

                } else if(selectedLanguage.equalsIgnoreCase(AppConstants.CHINESE)){
                    DialogChangeLan.tv_msg1.setText(getResources().getString(R.string.ToChange) + "\t" +getResources().getString(R.string.English));
                }
                break;
            case R.id.ll_Korea:
                changeLanguage();
                if(selectedLanguage.equalsIgnoreCase(AppConstants.ENGLISH)){
                    DialogChangeLan.tv_msg1.setText(getResources().getString(R.string.ToChange) + "\t" +getResources().getString(R.string.Korean));

                }else if(selectedLanguage.equalsIgnoreCase(AppConstants.KOREAN)){
                    DialogChangeLan.tv_msg1.setText(getResources().getString(R.string.ToChange) + "\t" +getResources().getString(R.string.Korean) + "\t" +getResources().getString(R.string.ToChangeKorea)
                    );

                } else if(selectedLanguage.equalsIgnoreCase(AppConstants.CHINESE)){
                    DialogChangeLan.tv_msg1.setText(getResources().getString(R.string.ToChange) + "\t" +getResources().getString(R.string.Korean));
                }                break;
        }
    }

}
