package com.example.vipha.vipha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;



public class Setting extends AppCompatActivity {
    Button btnLan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Button btnLan=(Button)findViewById(R.id.btnLan);

        btnLan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Setting.this,Setting2.class);
                startActivity(intent);
            }
        });
    }
}
