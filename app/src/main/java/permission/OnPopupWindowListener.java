package permission;

/*
 * Created by Huo Chhunleng on 10/17/2016.
 */
public interface OnPopupWindowListener {
    void onPopupPressed();
    void onPopupClose();
    //
}
